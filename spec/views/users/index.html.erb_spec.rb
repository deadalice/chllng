# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'users/index', type: :view do
  before(:each) do
    assign(:users, [
             User.create!(
               uid: 2,
               username: 'Username',
               firstname: 'Firstname',
               lastname: 'Lastname',
               locale: 'Locale'
             ),
             User.create!(
               uid: 2,
               username: 'Username',
               firstname: 'Firstname',
               lastname: 'Lastname',
               locale: 'Locale'
             )
           ])
  end

  it 'renders a list of users' do
    render
    assert_select 'tr>td', text: 2.to_s, count: 2
    assert_select 'tr>td', text: 'Username'.to_s, count: 2
    assert_select 'tr>td', text: 'Firstname'.to_s, count: 2
    assert_select 'tr>td', text: 'Lastname'.to_s, count: 2
    assert_select 'tr>td', text: 'Locale'.to_s, count: 2
  end
end
