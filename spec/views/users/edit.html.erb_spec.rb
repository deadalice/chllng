# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'users/edit', type: :view do
  before(:each) do
    @user = assign(:user, User.create!(
                            uid: 1,
                            username: 'MyString',
                            firstname: 'MyString',
                            lastname: 'MyString',
                            locale: 'MyString'
                          ))
  end

  it 'renders the edit user form' do
    render

    assert_select 'form[action=?][method=?]', user_path(@user), 'post' do
      assert_select 'input#user_uid[name=?]', 'user[uid]'

      assert_select 'input#user_username[name=?]', 'user[username]'

      assert_select 'input#user_firstname[name=?]', 'user[firstname]'

      assert_select 'input#user_lastname[name=?]', 'user[lastname]'

      assert_select 'input#user_locale[name=?]', 'user[locale]'
    end
  end
end
