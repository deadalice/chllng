# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'users/new', type: :view do
  before(:each) do
    assign(:user, User.new(
                    uid: 1,
                    username: 'MyString',
                    firstname: 'MyString',
                    lastname: 'MyString',
                    locale: 'MyString'
                  ))
  end

  it 'renders new user form' do
    render

    assert_select 'form[action=?][method=?]', users_path, 'post' do
      assert_select 'input#user_uid[name=?]', 'user[uid]'

      assert_select 'input#user_username[name=?]', 'user[username]'

      assert_select 'input#user_firstname[name=?]', 'user[firstname]'

      assert_select 'input#user_lastname[name=?]', 'user[lastname]'

      assert_select 'input#user_locale[name=?]', 'user[locale]'
    end
  end
end
