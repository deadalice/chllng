git reset --hard
git pull
bundle install --without development
#bundle update
rake db:migrate
rake assets:clean
rake assets:precompile
sudo cp -R /home/ec2-user/chllng/public/. /var/www/chllng.cc/
whenever --update-crontab
sudo supervisorctl restart foreman:foreman-web-1
rake telegram:bot:set_webhook
> log/production.log
tail -f log/production.log