Rails.application.routes.draw do

  telegram_webhook BotRouter

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :users, :controllers => {
    :omniauth_callbacks => "auth"
  }

  devise_scope :user do
    get 'sign_in', :to => 'devise/sessions#new', :as => :signin_path
    get 'sign_out', :to => 'devise/sessions#destroy', :as => :signout_path
  end

  #resources :challenges
  #match '/auth/:provider/callback', :to => 'auth#callback', :via => [:get, :post]

  authenticated :user do
    root 'home#index', as: 'authenticated_root'
  end
  devise_scope :user do
    root 'devise/sessions#new'
  end

  # Wrong route workaround
  match "/404", to: "errors#page_not_found", via: :all
  match "/500", to: "errors#internal_server_error", via: :all
  match "*path", to: "errors#page_not_found", via: :all
end
