Rails.application.config.middleware.use OmniAuth::Builder do
    provider :telegram, Telegram.bot.username, Telegram.bot.token
end