require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TelegramBotApp
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.enable_dependency_loading = true
    config.autoload_paths += %W[#{config.root}/lib]
    config.assets.initialize_on_precompile = false
    config.assets.enabled = true
    config.i18n.enforce_available_locales = false
    config.i18n.available_locales = %i[ru uk]
    config.i18n.default_locale = :ru
    config.i18n.fallbacks = [config.i18n.default_locale]
    # config.active_record.schema_format = :sql
    # config.active_record.record_timestamps = false
    config.active_record.default_timezone = :local
  end
end
