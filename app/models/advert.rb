class Advert < ApplicationRecord
  include Exlogger

  before_save :log_info
  acts_as_paranoid

  default_scope do
    where('? BETWEEN start AND finish', Time.current)
    .or(where(finish: nil))
    .order(Arel.sql('RANDOM()'))
  end

  class << self
    def get
      if (Advert.count > 0) && (rand(BotConst::Advert_Random) == 0)
        a = Advert.first
        a&.increment! :show_count
        return a
      end
    end
  end

  private

  def log_info
    _info self
  end
end
