class Comment < ApplicationRecord
  include Exlogger

  # NOTE: Comments belong to a user
  belongs_to :user
  before_save :log_info
  acts_as_paranoid

  class << self
    def get(postid)
      comment = Comment.where(postid: postid).size > BotConst::Comment_Limit ?
        I18n.t('bot_channel.skipped_comments', count: Comment.where(postid: postid).size - BotConst::Comment_Limit) :
        ''
      Comment.where(postid: postid).last(BotConst::Comment_Limit).map do |c|
        comment << "\n" if comment != ''
        #comment << I18n.t('bot_channel.comment', name: c.user.namelink, comment: c.text)
        #if c.user.uid == uid
        #  comment << I18n.t('bot_channel.comment_author', comment: c.text)
        #else
        comment << I18n.t('bot_channel.comment', comment: c.text)
        #end
      end
      return comment
    end
  end

  private

  def log_info
    _info self
  end
end
