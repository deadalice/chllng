class Challenge < ApplicationRecord
  include ActionView::Helpers::DateHelper
  include Exlogger

  acts_as_paranoid
  # has_one_attached :file

  belongs_to :user, optional: true
  has_many :posts, dependent: :nullify

  before_save :log_info
  before_destroy :log_info
  after_save :log_error
  after_create :log_info

  validates :title, presence: true, length: { in: 3..BotConst::Title_Size_Limit }
  validates :text, presence: true, length: { in: 3..BotConst::Text_Size_Limit }
  # validates :title, length: { in: 3..BotConst::TitleSizeLimit }
  # validates :text, length: { in: 3..BotConst::TextSizeLimit }
  validates :person_left, numericality: { greater_than_or_equal_to: 0 }

  scope :active, -> {
    (
      where('? BETWEEN start AND finish', Time.current)
      .where(Challenge.arel_table[:person_left].gt(0))
      .where('challenges.user_id IS NOT NULL')
    ).or(
      where('? < finish OR finish IS NULL', Time.current)
      .where('challenges.user_id IS NULL')
    )
  }

  scope :actual_for, ->(user) {
    joins("LEFT JOIN posts ON posts.challenge_id = challenges.id AND posts.user_id = #{user.id}")
    .where('posts.user_id IS NULL')
  }

  scope :admin, -> {
    active
    .where(user_id: nil)
  }

  scope :finished, -> {
    where(Challenge.arel_table[:finish].lt(Time.current))
    .or(where(Challenge.arel_table[:person_left].eq(0)))
    .where('user_id IS NOT NULL')
    .where(Challenge.arel_table[:finish].gt(Time.current - 1.day))
  }

  scope :almost_finished, -> {
    active
    .where(Challenge.arel_table[:finish].lt(Time.current + 2.hour))
    .where(Challenge.arel_table[:finish].gt(Time.current + 1.hour))
    #.or(where(Challenge.arel_table[:person_left].eq(1)))
  }

  def close!
    self.finish = Time.current
    save
  end

  def delete!
    if posts.size > 0
      self.close!
    else
      Telegram.bot.async(false) {
        Telegram.bot.delete_message chat_id: "@#{BotConst::Channel}", message_id: postid if postid
        Telegram.bot.delete_message chat_id: "@#{BotConst::Channel}", message_id: comments_postid if comments_postid
      }
      self.destroy!
    end
  end

  def formatted_title(post: nil, user: nil, icons: false, link: nil)
    s_title = title&.length > BotConst::Title_Size_Limit ?
      title[0..BotConst::Title_Size_Limit] + '...' : title
    if link && !post.deleted?
      caption = "<a href=\"t.me/#{BotConst::Channel}/#{link}\">#{s_title}</a>"
    else
      caption = s_title
    end

    if icons
      challenge_icon(post: post, user: user) + caption + time_with_icon.to_s + person_icon.to_s + rating_with_icon(post).to_s
    else
      challenge_icon(post: post, user: user) + caption
    end
  end

  def rating_with_icon(post)
    if post && (post.rates.size > 0)
      ' ' + I18n.t('items.rating') + ' ' + post.rating.to_s
    end
  end

  def formatted_text(user)
    # for future purposes
    description =
      text&.to_s.length > BotConst::Text_Size_Limit ?
      text&.to_s[0..BotConst::Text_Size_Limit] + I18n.t('buttons.read_more') : text&.to_s
    link = I18n.t('menu.post_link', link: "#{BotConst::Channel}/#{postid}") if !postid.nil?
    I18n.t('menu.challenge_text', text: (formatted_title + "\n\n" + description).strip,
      time: time_with_icon&.strip,
      person: person_icon,
      views: show_icon,
      autorenew: (autorenew ? " #{I18n.t('items.autorenew')} " : ""),
      link: link)
  end

  def add_person!
    self.user.decrement! :credit
    self.increment :person_left
    save!
  end

  def add_time!
    self.user.decrement! :credit
    self.finish = finish < Time.current ? Time.current +  BotConst::Time_To_Add : finish + BotConst::Time_To_Add
    save!
  end

  def showed!
    self.increment! :show_count
  end

  def get_file
    if (filetype == 'photo') && fileid
      file_path = (Telegram.bot.async(false) { Telegram.bot.get_file file_id: fileid }).dig('result', 'file_path')
      return "https://api.telegram.org/file/bot#{Telegram.bot.token}/#{file_path}"
    end
  end

  def actual?
    (Time.current > start.to_time) && (Time.current < finish.to_time) && (person_left > 0)
  end

  private

  def challenge_icon(post: nil, user: nil)
    if post&.deleted_at
      I18n.t('items.deleted_challenge')
    #elsif self.user.nil?
    #  I18n.t('items.regular_challenge')
    elsif !actual?
      I18n.t('items.inactive_challenge')
    else
      I18n.t('items.active_challenge')
    end
  end

  def person_icon
    if user.present? && actual?
      t = ' ' + I18n.t('items.person') + ' '
      t << posts.size.to_s + '/' if posts.size > 0
      t << (posts.size + person_left).to_s
    end
  end

  def time_with_icon
    if user && actual? && ((Time.current + 1.month) > finish.to_time)
      t0 = Time.current.to_i
      t1 = start.to_time.to_i
      t2 = finish.to_time.to_i
      clocks = I18n.t('items.clocks').chars
      return ' ' + clocks[(t0 - t1) * clocks.length / (t2 - t1)] + ' ' + distance_of_time_in_words(t0, t2)
    end
  end

  def show_icon
    return ' ' + I18n.t('items.show') + " #{show_count}" if self.user
  end

  def log_info
    _info self
  end

  def log_error
    _error self if errors.messages.any?
  end
end
