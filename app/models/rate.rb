class Rate < ApplicationRecord
  include Exlogger

  belongs_to :post
  belongs_to :user

  after_create :reward
  before_save :log_info
  after_save :log_error

  # Rating can be >1, but we are checking count with rate > 0
  scope :likes, -> {
    where(Rate.arel_table[:rate].gt(0))
  }
  scope :unlikes, -> {
    where(Rate.arel_table[:rate].lt(0))
  }

  def eval_rate!(new_rate)
    self.rate = rate == new_rate ? 0 : new_rate
    if rate && (rate < 0)
      rate = 0
    end
    save!
    self.post.user&.update rating: post.user&.rates.sum(:rate)
    self.post.update rating: post.rates.sum(:rate)
  end

  private

  def reward
    user&.increment! :credit
  end

  def log_info
    _info self
  end

  def log_error
    _error self if errors.messages.any?
  end
end
