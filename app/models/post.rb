class Post < ApplicationRecord
  include Exlogger

  acts_as_paranoid

  belongs_to :user
  belongs_to :challenge
  has_many :rates, dependent: :nullify

  after_create :reward
  before_create :log_info
  before_destroy :delete_post
  before_destroy :log_info
  after_save :log_error

  validates :text, presence: true, if: proc { |a| a.challenge.text_required }
  validates :fileid, presence: true, if: proc { |a| a.challenge.file_required }
  validates :filetype, presence: true, if: proc { |a| a.challenge.file_required }
  # validates :postid, presence: true
  # validates :text, length: { maximum: 1024 }

  scope :available, -> {
    where(Post.arel_table[:created_at].gt(Time.current - BotConst::Post_Expiration_Time))
    .includes(:challenge)
  }

  scope :top, -> {
    where(Post.arel_table[:rating].gt(0))
    .order(rating: :desc)
  }

  def expired?
    created_at < Time.current - BotConst::Edit_Expiration_Time
  end

  private

  def reward
    user.increment! :credit
    challenge.user&.increment! :rating
  end

  def log_info
    _info self
  end

  def log_error
    _error self if errors.messages.any?
  end

  def delete_post
    if postid
      Telegram.bot.delete_message chat_id: "@#{BotConst::Channel}",
        message_id: postid
    end
  end
end
