class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :omniauthable, :omniauth_providers => [:telegram]
  include ActionView::Helpers::DateHelper
  include Exlogger

  has_many :posts, dependent: :nullify
  has_many :rates, through: :posts
  has_many :challenges, dependent: :nullify

  before_save :log_info
  after_save :log_error
  after_create :log_info

  attr_accessor :email, :password
  attr_writer :login

  def login
    @login ||= self.uid
  end

  validates :uid, presence: true
  validates :credit, numericality: { greater_than_or_equal_to: 0 }
  #validates :password, presence: true, length: { in: 6..20 }, confirmation: true

  scope :best, -> {
    where(training_finished: true)
    .where(User.arel_table[:blocked_until].lt(Time.current))
    .order(rating: :desc)
  }

  def training?
    !training_finished
  end

  def namelink
    firstname.nil? ? "@#{username}" : "<a href=\"tg://user?id=#{uid}\">#{fullname}</a>"
  end

  def fullname
    "#{firstname} #{lastname}".strip
  end

  def block!(period)
    self.blocked_until = Time.current + period if self.blocked_until < Time.current + period
    self.challenges.map { |c| c.delete! }
    save
  end

  def blocked?
    blocked_until > Time.current if blocked_until
  end

  def rating_with_icon
    I18n.t('items.rating') + ' ' + rating.to_s
  end

  def credit_with_icon
    I18n.t('items.credit') + ' ' + credit.to_s
  end

  def header(item)
    item + I18n.t('menu.header', rating: rating, credit: credit) + "\n"
  end

  def degrade!
    self.rating = 0
    self.rating_position = nil
    self.rating_position_old = nil
    self.training_finished = false
    #save
    block! BotConst::Block_For_Block
  end

  private

  def self.from_omniauth(auth)
    where(uid: auth.uid, is_admin: true).first do |user|
      user.uid = auth.uid
      user.username = auth.username
      user.firstname = auth.first_name
      user.lastname = auth.last_name
      user.photo_url = auth.photo_url
      #user.password = Devise.friendly_token[0,20]
      #session['uid'] = auth.uid
    end
  end

  def email_required?
    false
  end

  def password_required?
    false
  end

  def will_save_change_to_email?
    false
  end

  def log_info
    _info self
  end

  def log_error
    _error self if errors.messages.any?
  end
end
