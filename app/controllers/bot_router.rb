module BotRouter
  module_function

  include Exlogger

  ExcludedCallbackQueries = %w[challenges rating add_time add_person langs autorenew_challenge]

  def dispatch(bot, update)
    message = update['message'] || update.dig('callback_query', 'message') || update['channel_post']
    return false unless message

    chat_type = message&.dig('chat', 'type')
    chat_name = message&.dig('chat', 'username')
    callback_query = update['callback_query']

    bot.delete_message chat_id: message.dig('chat', 'id'),
      message_id: message['message_id'] if chat_type && chat_type == 'private' && callback_query &&
      !ExcludedCallbackQueries.include?(update.dig('callback_query', 'data').split('|').first)

    if chat_type && chat_type == 'private'
      BotPrivateController.dispatch(bot, update)
    elsif chat_type && chat_type == 'channel' && chat_name == BotConst::Channel
      BotChannelController.dispatch(bot, update)
    elsif chat_type&.end_with?('group') && chat_name == BotConst::Channel
      BotGroupController.dispatch(bot, update)
    end
  end

  ### WORKAROUND

  def answer_callback_query(*)
    super
  rescue Telegram::Bot::Error => e
    raise unless e.message.include?('QUERY_ID_INVALID')
    exlog.warn "BotRouter: Ignoring telegram error: #{e.message}"
  end

  def edit_message(type, params = {})
    super
  rescue Telegram::Bot::Error => e
    raise unless e.message.include?('message is not modified')
    exlog.warn "BotRouter: Ignoring telegram error: #{e.message}"
  end

  def action_missing(action, *_args)
    if action_type == :command
      exlog.warn "BotRouter: Missing command: #{action_options[:command]}"
    else
      exlog.warn "BotRouter: Missing feature: #{action}"
    end
  end
end
