class AuthController < Devise::OmniauthCallbacksController
  skip_before_action :verify_authenticity_token

  def sign_in_with(provider_name)
    @user = User.from_omniauth(request.env["omniauth.auth"])
    if @user
      sign_in_and_redirect @user, :event => :authentication, notice: "Logged in!"
      session[:login] = @user.uid
      set_flash_message(:notice, :success, :kind => provider_name) if is_navigational_format?
    else
      redirect_to root_url, notice: "User is not registered!"
    end
  end

  def telegram
    sign_in_with "Telegram"
  end

  def destroy
    session.delete :login
    redirect_to root_url, notice: "Logged out!"
  end

  protected

  def callback
	  auth_hash = request.env['omniauth.auth']
	  render json: auth_hash
	end
end
