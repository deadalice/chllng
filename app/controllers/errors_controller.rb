class ErrorsController < ApplicationController
  def page_not_found
    logger.warn "Error 404: #{request.method} #{request.fullpath}"
    render :file => "public/404.html", :status => 404
  end

  def internal_server_error
    logger.warn "Error 500: #{request.method} #{request.fullpath}"
    render :file => "public/500.html",  :status => 500
  end
end