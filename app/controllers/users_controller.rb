class UsersController < ApplicationController
  before_action :authenticate_user!  
  before_action :set_user, only: [:show]

  attr_accessor :skip_password_validation

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
    session[:login] = @user&.uid
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:uid, :username, :firstname, :lastname, :blockeduntil, :locale)
  end

  protected

  def password_required?
    return false
  end
end
