class BotPrivateController < Telegram::Bot::UpdatesController
  include Telegram::Bot::UpdatesController::MessageContext
  include Telegram::Bot::UpdatesController::Session
  include ActionView::Helpers::DateHelper
  include Devise::Controllers::Helpers
  include BotCommon
  include BotKeyboards
  include Exlogger

  before_action :get_private_user

  def get_private_user
    @user = User.find_by(uid: from['id']) ||
            User.new(uid: from['id'],
                    username: from['username'],
                    firstname: from['first_name'],
                    lastname: from['last_name']
                    )
    if @user.new_record?
      #sign_in(@user)
      if Rails.env.development?
        @user.credit = 100
      end
      if I18n.available_locales.map(&:to_s).include? from['language_code']
        @user.locale = from['language_code']
      else
        @user.locale = I18n.default_locale
      end
      @user.save
      Telegram.bot.async(false) {
        respond_with :message,
          text: t('messages.start.welcome'),
          parse_mode: 'HTML', disable_web_page_preview: true
         }
    elsif
      if !@user.updated_at.today?
        @user.increment :credit
      end
      @user.username = from['username']
      @user.firstname = from['first_name']
      @user.lastname = from['last_name']
      @user.save if @user.changed?
    end
    I18n.locale = @user.locale.to_s[0..1] || I18n.default_locale
    session[:login] = @user.uid
  end

  def message(message)
    return nil if session[:timestamp] && session[:timestamp] > Time.current - 0.5.second

    session[:timestamp] = Time.current
    state = session[:state]
    session.delete :state

    if @user.training? && message['photo'].nil?
      return respond_with(:message,
        text: t('messages.start.first_challenge'), reply_markup: { remove_keyboard: true })
    elsif @user.blocked?
      return respond_with(:message, 
        text: t('errors.blocked_until', until: distance_of_time_in_words(Time.current, @user.blocked_until)))
    end

    # Override button pressing for any state
    case message['text']
      # Challenges
      when t('buttons.main.list')
        session_clear
        return list!
      # Rating
      when t('buttons.main.rating')
        session_clear
        return rating!
    end

    # Commentaries
    if session[:comment_message_id] && message['text'] && (message['text'].length > 0)
      return respond_with(:message, text:
        t('errors.comment_too_long')) unless message['text'].length < BotConst::Comment_Size_Limit
      if add_comment(message['text'])
        send_to_telegram chat_id: @user.uid, text: t('messages.comment_added',
          posturl: BotConst::Channel + '/' + session[:comment_message_id].to_s)
      end
      return session.delete :comment_message_id
    end

    # If state is set, process state instead if code
    case state
      when 'new_challenge_text'
        if !message['photo'].nil?
          session[:filetype] = 'photo'
          session[:fileid] = message['photo'][-1]['file_id']
          session[:text] = message['caption']
        elsif !message['animation'].nil?
          session[:filetype] = 'animation'
          session[:fileid] = message['animation']['file_id']
          # There is no caption for animation
          session[:text] = ''
        elsif !message['video'].nil?
          session[:filetype] = 'video'
          session[:fileid] = message['video']['file_id']
          # There is no caption for video
          session[:text] = ''
        elsif !message['voice'].nil?
          session[:filetype] = 'voice'
          session[:fileid] = message['voice']['file_id']
          # There is no caption for voice
          session[:text] = ''
        elsif !message['video_note'].nil?
          session[:filetype] = 'video_note'
          session[:fileid] = message['video_note']['file_id']
          # There is no caption for video_note
          session[:text] = ''
        elsif
          session[:text] = message['text']
        else
          return respond_with(:message, text: t('errors.text_required')) unless message['text'] && (message['text'] != '')
        end
        session[:state] = 'new_challenge_title'
        return send_to_telegram(chat_id: @user.uid, text: t('messages.challenge.title'))
      when 'new_challenge_title'
        return answer_callback_query(t('errors.not_enough_credit'),
          show_alert: true) unless @user.reload.credit > 0
        return respond_with(:message, text: t('errors.text_required')) unless message['text'] && (message['text'] != '')
        return respond_with(:message, text: t('errors.challenge_already_going')) unless @user.challenges.active.count < BotConst::Max_Challenges
        #@user.decrement! :credit
        session[:title] = message['text']
        c = create_challenge
        send_to_telegram chat_id: @user.uid, text: t('messages.challenge.saved')
        return show c
      when 'skip_text'
        session[:text] = message['text'][0..BotConst::Text_Size_Limit]
        if session[:current_challenge] # && Challenge.actual.where(id: session[:current_challenge]).any?
          return show Challenge.find(session[:current_challenge])
        else
          return list!
        end
      when 'challenge'
        # Just skip session clear
      when 'post_advert_with_rating'
        a = Advert.create(
          text: message['text'][0..BotConst::Text_Size_Limit],
          user: @user,
          start: Time.current,
          finish: Time.current + 1.month,
          approved: true
        )
        send_to_telegram chat_id: BotConst::Admin, text: t('menu.admin_new_advert', user: @user.namelink, advert: a.text)
        return send_to_telegram chat_id: @user.uid, text: t('messages.advert_saved')
      else
        session_clear
    end

    # Content processing
    if !message['photo'].nil?
      session[:filetype] = 'photo'
      session[:fileid] = message['photo'][-1]['file_id']
      session[:text] = message['caption']
      if @user.training? && create_answer(Challenge.admin.find(BotConst::Challenge_Start_Id))
        @user.update_attributes(training_finished: true)
        return respond_with :message, text: t('messages.start.finish'),
          parse_mode: 'HTML', disable_web_page_preview: true,
          reply_markup: {
           keyboard: start_keyboard,
           resize_keyboard: true,
           one_time_keyboard: false
           }
      end
    elsif !message['animation'].nil?
      session[:filetype] = 'animation'
      session[:fileid] = message['animation']['file_id']
    elsif !message['video'].nil?
      session[:filetype] = 'video'
      session[:fileid] = message['video']['file_id']
    elsif !message['voice'].nil?
      session[:filetype] = 'voice'
      session[:fileid] = message['voice']['file_id']
    elsif !message['video_note'].nil?
      session[:filetype] = 'video_note'
      session[:fileid] = message['video_note']['file_id']
    elsif !message['text'].nil?
      session[:text] = message['text']
    elsif !message['document'].nil?
      return respond_with :message, text: t('errors.file_not_image')
    else
      send_to_telegram chat_id: BotConst::Admin, text: t('errors.format', message: self.update.to_s)
      return respond_with :message, text: t('errors.bad_content')
    end

    # Finals for challenge
    if session[:text].nil?
      session[:state] = 'skip_text'
      return send_to_telegram chat_id: @user.uid, text: t('messages.post.add_caption', limit: BotConst::Text_Size_Limit),
        keyboard: skip_text_keyboard
    elsif session[:current_challenge]
      show Challenge.find(session[:current_challenge])
    else
      list!
    end
  end

  def callback_query(data)
    return answer_callback_query t('errors.blocked_until',
      until: distance_of_time_in_words(Time.current, @user.blocked_until)), show_alert: true if @user.blocked?

    state = data.split('|')[0]
    code = data.split('|')[1]
    case state
    when 'langs'
      edit_message :reply_markup, reply_markup: { inline_keyboard: lang_keyboard }
    when 'lang'
      @user.locale = code
      @user.save
      I18n.locale = @user.locale || I18n.default_locale
      answer_callback_query t('messages.locale_changed')
      start!
    when 'help'
      help!
    when 'challenge'
      c = Challenge.find_by(id: code.to_i)
      answer_callback_query t('errors.challenge_not_found'), show_alert: true unless c.present?
      show c
      session[:current_challenge] = c.id unless c.posts.find_by(user: @user) || c.user == @user
    when 'answer'
      c = Challenge.active.find_by(id: code.to_i)
      return answer_callback_query(t('errors.expired_challenge'),
        show_alert: true) unless c
      return answer_callback_query(t('errors.already_answered'),
        show_alert: true) if c.posts.exists?(user: @user)
      if p = create_answer(c)
        send_to_telegram chat_id: @user.uid, text: t('messages.post.saved',
          posturl: BotConst::Channel + '/' + p.postid.to_s) #swer, keyboard: answer_keyboard(p)
      else
        p&.destroy
      end
      return session.delete :state # To solve issue with last answer reselect on answer
    when 'challenges'
      list! code.to_i
    when 'back_to'
      case session[:menuitem]
        when 'rating'
          rating!
      else
        list!
      end
    when 'new_challenge'
      return answer_callback_query(t('errors.not_enough_rating'),
        show_alert: true) if @user.reload.credit < 5
      if session[:text] || session[:filetype]
        state = 'new_challenge_title'
        respond_with :message, text: t('messages.challenge.title')
      else
        state = 'new_challenge_text'
        respond_with :message, text: t('messages.challenge.text')
      end
    when 'autorenew_challenge'
      return answer_callback_query(t('errors.not_enough_credit'),
        show_alert: true) unless @user.reload.credit > 0
      if c = @user.challenges.find_by(id: code.to_i)
        c.autorenew = !c.autorenew
        c.save
        edit_challenge(c.reload)
      end
    when 'delete_challenge'
      @user.challenges.find_by(id: code.to_i)&.destroy!
      answer_callback_query t('notifications.challenge_deleted'), show_alert: true
      return rating!
    when 'add_time'
      return answer_callback_query(t('errors.not_enough_credit'),
        show_alert: true) unless @user.reload.credit > 0
      if c = @user.challenges.find_by(id: code.to_i)
        if !c.actual? && (@user.challenges.active.count >= BotConst::Max_Challenges)
          return respond_with(:message, text: t('errors.challenge_already_going'))
        end
        if c.finish < Time.current + 1.week
          c.add_time!
        end
        edit_challenge(c)
      end
    when 'add_person'
      return answer_callback_query(t('errors.not_enough_credit'),
        show_alert: true) unless @user.reload.credit >= 0
      if c = @user.challenges.find_by(id: code.to_i)
        if !c.actual? && (@user.challenges.active.count >= BotConst::Max_Challenges)
          return respond_with(:message, text: t('errors.challenge_already_going'))
        end
        c.add_person!
        edit_challenge(c)
      end
    when 'skip_text'
      if session[:current_challenge] #&& Challenge.active.where(id: session[:current_challenge]).any?
        show Challenge.actual_for(@user).find_by(id: session[:current_challenge])
      else
        list!
      end
    when 'delete_answer'
      @user.post.find_by(id: code.to_i)&.destroy
      return answer_callback_query(t('notifications.answer_deleted'), show_alert: true)
    end
    session[:state] = state
  end

  ### HANDLERS

  def start!(ref_id = nil)
    return respond_with(:message, text: t('errors.blocked_until',
      until: distance_of_time_in_words(Time.current, @user.blocked_until))) if @user.blocked?

    if @user.training?
      respond_with :message, text: t('messages.start.first_challenge'),
        reply_markup: { remove_keyboard: true }
      if u = User.find_by(uid: ref_id)
        @user.ref_user_id = ref_id
        @user.save
        I18n.locale = @user.locale.to_s[0..1] || I18n.default_locale
        send_to_telegram chat_id: ref_id, text: t('messages.ref_register', name: u.namelink)
      end
    else
      respond_with :message, text: t('messages.keyboard_updated'), reply_markup: {
        keyboard: start_keyboard,
        resize_keyboard: true,
        one_time_keyboard: false
      }
      list!
    end
  end

  def list!(position = nil)
    return respond_with(:message, text: t('errors.blocked_until',
      until: distance_of_time_in_words(Time.current, @user.blocked_until))) if @user.blocked?
    return nil if @user.training?

    if session[:text].nil? && (session[:state] != 'skip_text')
      text = @user.header t('buttons.main.list')
      posts = []
      Post.available.select { |p| (p.user != @user) && !p.rates.exists?(user: @user) }.map do |p|
        posts << p.challenge.formatted_title(post: p, link: p.postid)
      end
      if posts.any?
        text << t('menu.not_rated_answers', list: posts.join(' '))
      else
        # !!!
      end
    else
      text = t('menu.answer')
      for_answer = true
    end

    challenges = []
    Challenge.active.actual_for(@user)
      .map do |c|
        if for_answer || (c.user != @user)
          challenges << [{ text: c.formatted_title(user: @user, icons: true), callback_data: 'challenge|' + c.id.to_s }]
        end
      end

    text = text.strip + t('menu.pin', uid: @user.uid)

    if position
      edit_message :text, text: text,
        parse_mode: 'HTML', disable_web_page_preview: true,
        reply_markup: { inline_keyboard: list_keyboard(position.to_i, challenges) }
    else
      advertisement! unless for_answer
      send_to_telegram chat_id: @user.uid, text: text, keyboard: list_keyboard(0, challenges)
    end

    session[:menuitem] = 'list'
  end

  def rating!
    return respond_with(:message, text: t('errors.blocked_until',
      until: distance_of_time_in_words(Time.current, @user.blocked_until))) if @user.blocked?
    return nil if @user.training?

    text = @user.header t('buttons.main.rating')
    text << t('menu.statistics',
      pos: (@user.rating_position || 0),
      users: User.best.size,
      challenges: @user.challenges.with_deleted.size,
      answers: @user.posts.with_deleted.size,
      current_challenges: @user.challenges.active.size
    )

    answered = []
    Post.available.select { |p| (p.user == @user) }.map do |p|
      answered << p.challenge.formatted_title(icons: true, link: p.postid, post: p)
    end
    text << t('menu.answered', list: answered.join("\n")) if !answered.empty?

    text = text.strip + t('menu.pin', uid: @user.uid)

    challenges = []
    @user.challenges.map do |c|
      challenges << [{ text: c.formatted_title(user: @user, icons: true), callback_data: 'challenge|' + c.id.to_s }]
    end
    if challenges.size < BotConst::Max_Challenges
      (BotConst::Max_Challenges - challenges.size).times { challenges << button_add_challenge }
    end
    advertisement!
    send_to_telegram chat_id: @user.uid, text: text, keyboard: rating_keyboard(challenges)

    session[:menuitem] = 'rating'
  end

  def help!
    send_to_telegram chat_id: @user.uid, text: t('messages.help'), keyboard: back_keyboard
  end

  ### WORKAROUND

  def answer_callback_query(*)
    super
  rescue Telegram::Bot::Error => e
    raise unless e.message.include?('QUERY_ID_INVALID')
    exlog.warn "BotPrivateController: Ignoring telegram error: #{e.message}"
  end

  def edit_message(type, params = {})
    super
  rescue Telegram::Bot::Error => e
    raise unless e.message.include?('message is not modified')
    exlog.warn "BotPrivateController: Ignoring telegram error: #{e.message}"
  end

  def action_missing(action, *_args)
    if action_type == :command
      exlog.warn "BotPrivateController: Missing command: #{action_options[:command]}"
    else
      exlog.warn "BotPrivateController: Missing feature: #{action}"
    end
  end
end
