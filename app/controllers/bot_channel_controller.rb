class BotChannelController < Telegram::Bot::UpdatesController
  include Telegram::Bot::UpdatesController::MessageContext
  include Telegram::Bot::UpdatesController::Session
  include ActionView::Helpers::DateHelper
  include BotCommon
  include BotKeyboards
  include Exlogger
  require 'date'

  before_action :get_channel_user

  def get_channel_user
    if from
      @user = User.find_by(uid: from['id'])
      I18n.locale = @user ? @user.locale.to_s[0..1] : from['language_code'][0..1]
    end
  end

  def callback_query(data)
    return answer_callback_query t('errors.not_registered'), show_alert: true unless @user&.training_finished?
    return answer_callback_query t('errors.blocked_until',
      until: distance_of_time_in_words(Time.current, @user.blocked_until)), show_alert: true if @user.blocked?
    return answer_callback_query t('errors.expired_comments'), show_alert: true if Time.
      at(self.update.dig('callback_query', 'message', 'date')) < Time.current - BotConst::Edit_Expiration_Time

    case data.split('|')[0]
      when 'challenge'
        c = Challenge.find(data.split('|')[1].to_i)
        answer_callback_query t('errors.challenge_not_found'), show_alert: true unless c.present?
        answer_callback_query t('notifications.challenge_showing'), show_alert: true
        show c
      when 'like'
        like data.split('|')[1].to_i, 1
      when 'unlike'
        like data.split('|')[1].to_i, -1
      when 'add_comment'
        session[:comment_message_id] = self.update.dig('callback_query', 'message', 'message_id')
        #session[:comment_user_id] = data.split('|')[1].to_i
        answer_callback_query t('notifications.new_comment_text')
        async_send_to_telegram chat_id: @user.uid, text: t('messages.new_comment_text', limit: BotConst::Comment_Size_Limit)
      else
        answer_callback_query t('errors.undefined'), show_alert: true
    end
  rescue ActiveRecord::RecordNotFound => e
    answer_callback_query t('errors.id_not_found'), show_alert: true
    exlog.warn "#{e.message}"
#  rescue Exception => e
#    exlog.error "#{e.message}"
  end

  def channel_post(post)
    #create_comment_base!
  end

  def like(post_id, rate)
    if p = Post.find(post_id)
      r = p.rates.find_by(user: @user) || p.rates.new(user: @user)
      return answer_callback_query t('errors.too_often'),
        show_alert: true if r.updated_at && r.updated_at > Time.current - BotConst::Operation_Time_Limit
      return answer_callback_query t('errors.no_time_left') if p.expired?

      if p.user == @user
        if rate > 0
          return answer_callback_query t('errors.own_answer')
        else
          p.destroy
          answer_callback_query t('notifications.answer_deleted'),
            show_alert: true
        end
      else
        r.eval_rate! rate
        if p.rating.to_i <= BotConst::Rate_Limit_For_Block
          p.user.block! BotConst::Block_For_Bad_Post
          p.destroy
        elsif r.rate == 0
          answer_callback_query t('notifications.rate_deleted')
          edit_message :reply_markup, reply_markup: { inline_keyboard: post_keyboard(p) }
        else
          answer_callback_query t('notifications.rate_accepted')
          edit_message :reply_markup, reply_markup: { inline_keyboard: post_keyboard(p) }
        end
      end
    else
      answer_callback_query t('errors.id_not_found'), show_alert: true
    end
  end

  ### WORKAROUND

  def answer_callback_query(*)
    super
  rescue Telegram::Bot::Error => e
    raise unless e.message.include?('QUERY_ID_INVALID')
    exlog.warn "BotChannelController: Ignoring telegram error: #{e.message}"
  end

  def edit_message(type, params = {})
    super
  rescue Telegram::Bot::Error => e
    raise unless e.message.include?('message is not modified')
    exlog.warn "BotChannelController: Ignoring telegram error: #{e.message}"
  end
end
