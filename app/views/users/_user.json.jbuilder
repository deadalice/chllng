# frozen_string_literal: true

json.extract! user, :id, :uid, :username, :firstname, :lastname, :blockeduntil, :locale, :created_at, :updated_at
json.url user_url(user, format: :json)
