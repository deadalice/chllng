ActiveAdmin.register User do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :blocked_until, :locale, :training_finished, :is_admin, :rating, :credit, :ref_user_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:uid, :username, :firstname, :lastname, :blocked_until, :locale, :training_finished, :is_admin, :rating, :credit, :rating_position, :rating_position_old, :ref_user_id, :email, :encrypted_password, :telegram_hash, :photo_url, :token, :secret, :provider]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

end
