ActiveAdmin.register Advert do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  config.sort_order = "id_desc"
  permit_params :text, :link, :start, :finish, :approved
  #
  # or
  #
  # permit_params do
  #   permitted = [:text, :link, :start, :finish, :show_count, :approved, :deleted_at, :user_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

end
