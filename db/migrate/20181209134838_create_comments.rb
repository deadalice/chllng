class CreateComments < ActiveRecord::Migration[6.0]
  def self.up
    change_table :comments do |t|
      t.references :commentable, :polymorphic => true
      t.string :role, :default => "comments"
    end

    add_index :comments, :commentable_type
    add_index :comments, :commentable_id
  end

  def self.down
    drop_table :comments
  end
end
