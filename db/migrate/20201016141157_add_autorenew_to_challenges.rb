class AddAutorenewToChallenges < ActiveRecord::Migration[6.0]
  def change
    add_column :challenges, :autorenew, :boolean, default: true
  end
end
