class AddAdvertsV2 < ActiveRecord::Migration[6.0]
  def change
    create_table :adverts do |t|
      t.text :text, null: false
      t.string :link
      t.datetime :start, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.datetime :finish
      t.integer :show_count, null: false, default: -> { 0 }
      t.boolean :approved, null: false, default: false

      t.datetime :deleted_at
      t.datetime :created_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.datetime :updated_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
    add_index :adverts, :deleted_at
  end
end
