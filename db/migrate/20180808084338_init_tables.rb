class InitTables < ActiveRecord::Migration[6.0]
  def change
    # users
    create_table :users do |t|
      t.bigint :uid, index: { unique: true }
      t.string :username, limit: 32
      t.string :firstname, limit: 255
      t.string :lastname, limit: 255
      t.datetime :blocked_until, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.string :locale, limit: 10
      t.boolean :training_finished, null: false, default: false
      t.boolean :is_admin, null: false, default: false

      t.integer :rating, null: false, default: -> { 0 }
      t.integer :credit, null: false, default: -> { 0 }

      t.integer :rating_position
      t.integer :rating_position_old
      t.integer :ref_user_id

      t.datetime :created_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.datetime :updated_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    # posts
    create_table :posts do |t|
      t.text :text
      t.string :fileid
      t.string :filetype
      t.integer :postid, index: { unique: true }
      t.integer :comments_postid, index: { unique: true }
      t.integer :rating

      t.datetime :deleted_at
      t.datetime :created_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.datetime :updated_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.belongs_to :user, index: true
      t.belongs_to :challenge, index: true
    end
    # add_reference :posts, :users, foreign_key: true
    add_index :posts, %i[user_id challenge_id]
    add_index :posts, :deleted_at
    add_index :posts, :updated_at

    # challenges
    create_table :challenges do |t|
      t.string :title, null: false, index: { unique: true }
      t.text :text
      t.string :fileid
      t.string :filetype
      t.datetime :start
      t.datetime :finish
      t.integer :show_count, null: false, default: -> { 0 }
      t.integer :postid, index: { unique: true }
      t.integer :comments_postid, index: { unique: true }
      t.integer :person_left, null: false, default: -> { 0 }

      # validations
      t.boolean :text_required, null: false, default: -> { false }
      t.boolean :file_required, null: false, default: -> { false }

      t.datetime :deleted_at
      t.datetime :created_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.datetime :updated_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }

      # if no :user -> this challenge was created by admin
      t.belongs_to :user, index: true, optional: true
    end
    # add_reference :challenges, :posts, foreign_key: true
    add_index :challenges, :updated_at
    add_index :challenges, :deleted_at

    # rates
    create_table :rates do |t|
      t.integer :rate, null: false, default: -> { 0 }

      t.datetime :created_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.datetime :updated_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.belongs_to :user, index: true
      t.belongs_to :post, index: true
    end
    add_index :rates, %i[user_id post_id]
    # add_reference :rates, :users, foreign_key: true
    # add_reference :rates, :posts, foreign_key: true

    # comments
    create_table :comments do |t|
      t.string :text, null: false
      t.integer :postid, null: false

      t.datetime :deleted_at
      t.datetime :created_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.datetime :updated_at, null: false, default: -> { 'CURRENT_TIMESTAMP' }
      t.belongs_to :user, index: true, optional: true
    end
    add_index :comments, :deleted_at
  end
end