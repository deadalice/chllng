# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Challenge.create(title: "Обучение", 
                 text: "Обучение",
                 start: Time.current, finish: Time.current + 10.years,
                 person_left: 0, file_required: true)
Advert.create(text: "<i>⭐ - Рейтинг отображается в таблице лучших, зарабатывается оценками и другими действиями.\n" +
  "🌕 - Карма используется для ответов и заданий, восстанавливается оценками чужих заданий или со временем.</i>",
  start: Time.current, finish: Time.current + 10.years)

if Rails.env.development?
  u1 = User.create(uid: -1)
  u2 = User.create(uid: -2)
  Challenge.create(title: 'Test1',
                   text: 'Test',
                   filetype: 'photo',
                   fileid: 'AgACAgIAAxkBAAJC4l7X4u2KNW__BmClXnV6hWQS2WGAAAIgrDEbKp3ASrN4Y042_cgE6eT5lC4AAwEAAwIAA3kAA6NjAAIaBA',
                   start: '2018-08-26 10:30:14'.to_time,
                   finish: '2028-11-29 10:30:14'.to_time, person_left: 10, user: u1)
  c = Challenge.create(title: 'Test2', text: 'Test', start: '2018-08-26 10:30:14'.to_time, finish: '2028-11-29 10:30:14'.to_time, person_left: 10, user: u1)
  Post.create text: 'Test10', rating: 3, user: u1, challenge: c
  Post.create text: 'Test11', rating: 7, user: u2, challenge: c

  Challenge.create title: 'Test3', text: 'Test', filetype: 'photo', fileid: 'AgACAgIAAxkBAAJC4l7X4u2KNW__BmClXnV6hWQS2WGAAAIgrDEbKp3ASrN4Y042_cgE6eT5lC4AAwEAAwIAA3kAA6NjAAIaBA',
                   start: '2018-08-26 10:30:14'.to_time, finish: '2028-11-10 10:30:14'.to_time, person_left: 4, user: u2
  Challenge.create title: 'Test4', text: 'Test', filetype: 'photo', fileid: 'AgACAgIAAxkBAAJC4l7X4u2KNW__BmClXnV6hWQS2WGAAAIgrDEbKp3ASrN4Y042_cgE6eT5lC4AAwEAAwIAA3kAA6NjAAIaBA',
                   start: '2018-08-26 10:30:14'.to_time, finish: '2028-11-29 10:30:14'.to_time, person_left: 10, user: u2
  Challenge.create title: 'Test5', text: 'Test', start: '2018-07-26 10:30:14'.to_time, finish: '2018-11-29 10:30:14'.to_time, person_left: 10, user: u2
  Challenge.create title: 'Test6', text: 'Test', filetype: 'photo', fileid: 'AgACAgIAAxkBAAJC4l7X4u2KNW__BmClXnV6hWQS2WGAAAIgrDEbKp3ASrN4Y042_cgE6eT5lC4AAwEAAwIAA3kAA6NjAAIaBA',
                   start: '2018-08-26 10:30:14'.to_time, finish: '2028-11-29 10:30:14'.to_time, person_left: 10, user: u1
  Challenge.create title: 'Test7', text: 'Test', filetype: 'photo', fileid: 'AgACAgIAAxkBAAJC4l7X4u2KNW__BmClXnV6hWQS2WGAAAIgrDEbKp3ASrN4Y042_cgE6eT5lC4AAwEAAwIAA3kAA6NjAAIaBA',
                   start: '2018-08-26 10:30:14'.to_time, finish: '2018-11-29 10:30:14'.to_time, person_left: 10, user: u1
end
