namespace :users do
  desc "Calculate top users"
  task calc_top: :environment do
    include BotCommon
    text = ''
    pos = 0
    User.best.map do |u|
      pos += 1
      u.rating_position = pos
      u.save
    end
  end
end

namespace :users do
  desc "Show top users in channel"
  task show_top: :environment do
    include BotCommon
    text = ''
    pos = 0
    User.best.limit(15).map do |u|
      pos += 1
      u.rating_position = pos
      u.save
      text << "\n" + pos.to_s + '. ' + u.namelink + " " + u.rating_with_icon
    end
    async_send_to_telegram chat_id: "@#{BotConst::Channel}",
      text: I18n.t('bot_channel.weekly_top', list: text), keyboard: bot_url_keyboard
  end
end

namespace :challenges do
  desc "Send message to channel with top answers and send link to author to top user"
  task close: :environment do
    include ActionView::Helpers::DateHelper
    include BotCommon
    text = ''
    Challenge.finished.map do |c|
      text << "\n\n" + c.formatted_title(link: c.postid) +
      I18n.t('bot_channel.close_template',
        users: c.posts.with_deleted.size,
        period: distance_of_time_in_words(c.start, c.finish),
        rating: c.posts.with_deleted.sum('rating')
      )
    end
    async_send_to_telegram chat_id: "@#{BotConst::Channel}",
      text: I18n.t('bot_channel.challenge_top', list: text) if text != ''
  end
end

namespace :challenges do
  desc "Reminder for challenges that almost finished"
  task almost_finished: :environment do
    include BotCommon
    Challenge.almost_finished.map do |c|
      if c.autorenew && c.user.credit > 0
        c.add_time!
      else
        async_send_to_telegram chat_id: c.user.uid,
          text: I18n.t('messages.reminder.almost_finished',
          challenge: c.formatted_title,
          users: c.posts.with_deleted.size,
          rating: c.posts.with_deleted.sum('rating'))
      end
    end
  end
end
