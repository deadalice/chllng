module BotCommon
  include BotKeyboards
  include Exlogger

  ### FUNCTIONS

  def send_to_telegram(args)
    content = {}
    content[:disable_web_page_preview] = !args.fetch(:preview, false)
    keyboard = args.fetch(:keyboard, nil)
    content[:chat_id] = args.fetch(:chat_id)
    content[:reply_markup] = { inline_keyboard: keyboard } if keyboard
    content[:parse_mode] = 'HTML'
    text = args.fetch(:text, nil)
    case args.fetch(:filetype, '')
      when 'photo'
        content[:photo] = args.fetch(:fileid)
        content[:caption] = text if text
        Telegram.bot.send_photo content
      when 'animation'
        content[:animation] = args.fetch(:fileid)
        content[:caption] = text if text
        Telegram.bot.send_animation content
      when 'video'
        content[:video] = args.fetch(:fileid)
        content[:caption] = text if text
        Telegram.bot.send_video content
      when 'voice'
        content[:voice] = args.fetch(:fileid)
        content[:caption] = text if text
        Telegram.bot.send_voice content
      when 'video_note'
        content[:video_note] = args.fetch(:fileid)
        content[:caption] = text if text
        Telegram.bot.send_video_note content
      else
        content[:text] = text if text
        Telegram.bot.send_message content
    end
  end

  def async_send_to_telegram(args)
    Telegram.bot.async(false) { send_to_telegram(args) }
  rescue Telegram::Bot::Forbidden
    exlog.warn("Forbidden: " + args.to_s)
    send_to_telegram chat_id: BotConst::Admin, text: I18n.t('errors.format', message: args.to_s)
    if chat_id = args.fetch(:chat_id)
      User.find_by(uid: chat_id.to_i)&.degrade!
    end
  end

  def error_send_to_telegram(args)
    send_to_telegram(args)
    send_to_telegram chat_id: BotConst::Admin, text: I18n.t('errors.format', message: args.fetch(:chat_id).to_s + ' ' + args.fetch(:text, ''))
  end

  ### METHODS

  def session_clear
    # Think to use reset_session instead
    session.delete :filetype
    session.delete :fileid
    session.delete :text
    session.delete :title
    session.delete :send_to_telegram
    session.delete :current_challenge
    session.delete :comment_message_id
    session.delete :comment_user_id
    session.delete :state
  end

  def show(challenge)
    return nil unless challenge
    session.delete :current_challenge
    challenge.showed!
    async_send_to_telegram chat_id: @user.uid, filetype: challenge.filetype,
      fileid: challenge.fileid, text: challenge.formatted_text(@user),
      keyboard: show_challenge_keyboard(challenge)
  end

  def create_answer(challenge)
    answer_text = @user.training? ? t('messages.start.lets_welcome', name: @user.namelink) : escape(session[:text])

    p = Post.create(challenge: challenge,
      user: @user,
      filetype: session[:filetype],
      fileid: session[:fileid],
      text: answer_text)

    if challenge.person_left > 0
      challenge.decrement! :person_left
      if challenge.person_left == 0
        if challenge.autorenew && (challenge.user&.credit > 0)
          challenge.user&.decrement! :credit
          challenge.add_person!
        else
          send_to_telegram chat_id: challenge.user.uid, text: t('messages.reminder.finished',
            challenge: challenge.formatted_title,
            users: challenge.person_left,
            rating: challenge.rating)
        end
      end
    end

    p.postid = async_send_to_telegram(chat_id: "@#{BotConst::Channel}",
      text: answer_text,
      filetype: p.filetype,
      fileid: p.fileid,
      preview: true, keyboard: post_keyboard(p)).dig('result', 'message_id').to_i

    if p.postid
      # p.comments_postid = create_comment_base!
      p.save
      if challenge.user && (challenge.user.uid > 0) && (challenge.user != @user)
        send_to_telegram chat_id: challenge.user.uid, text: t('messages.new_answer',
          title: challenge.formatted_title,
          posturl: BotConst::Channel + '/' + p.postid.to_s,
          left: challenge.person_left)
      end
      # TODO: Add check if finished
      #async_send_to_telegram chat_id: c.user.uid,
      #  text: I18n.t('messages.reminder.finished',
      #  challenge: c.formatted_title,
      #  users: c.posts.with_deleted.size,
      #  rating: c.posts.with_deleted.sum('rating'))
    else
      p.really_destroy!
      error_send_to_telegram(chat_id: @user.uid, text: t('errors.something_wrong'))
    end
    session_clear
    return p
  end

  def edit_challenge(challenge)
    if challenge.filetype
      edit_message :caption, caption: challenge.formatted_text(@user), disable_web_page_preview: true, parse_mode: 'HTML',
        reply_markup: { inline_keyboard: show_challenge_keyboard(challenge) }
    else
      edit_message :text, text: challenge.formatted_text(@user), disable_web_page_preview: true, parse_mode: 'HTML',
        reply_markup: { inline_keyboard: show_challenge_keyboard(challenge) }
    end
  end

  def create_challenge
    c = Challenge.create(user: @user,
                    title: session[:title],
                    text: escape(session[:text]),
                    filetype: session[:filetype],
                    fileid: session[:fileid],
                    start: Time.current,
                    finish: Time.current + BotConst::Time_To_Add,
                    person_left: BotConst::Person_To_Add)
    if c.postid = async_send_to_telegram(chat_id: "@#{BotConst::Channel}",
          text: (c.formatted_title + "\n\n" + c.text.to_s).strip,
          filetype: session[:filetype],
          fileid: session[:fileid]).dig('result', 'message_id').to_i
      #c.comments_postid = create_comment_base!
      c.save
    else
      c.destroy!
      error_send_to_telegram(chat_id: @user.uid, text: t('errors.something_wrong'))
    end
    session_clear
    return c
  rescue ActiveRecord::RecordNotUnique
    send_to_telegram(chat_id: @user.uid, text: t('errors.already_exists'))
  end

  def advertisement!
    if a = Advert.get
      async_send_to_telegram chat_id: @user.uid, text: I18n.t('menu.advert', text: a.text, link: a.link).strip
    end
  end

  def create_comment_base!
    return async_send_to_telegram(chat_id: "@#{BotConst::Channel}", text: I18n.t('bot_channel.comments_header'),
      keyboard: comment_keyboard).dig('result', 'message_id').to_i
  end

  def add_comment(text)
    if Comment.create(postid: session[:comment_message_id], user: @user, text: escape(text))
      Telegram.bot.edit_message_text chat_id: "@#{BotConst::Channel}",
          message_id: session[:comment_message_id],
          text: Comment.get(session[:comment_message_id]),
          disable_web_page_preview: true,
          parse_mode: 'HTML',
          reply_markup: { inline_keyboard: comment_keyboard }
      users = [@user.id]
      Comment.where(postid: session[:comment_message_id]).last(BotConst::Comment_Limit).map do |c|
        if (!users.include? c.user.id)
          send_to_telegram chat_id: c.user.uid, text: t('messages.new_comment',
            posturl: BotConst::Channel + '/' + c.postid.to_s)
          users << c.user.id
        end
      end
      if c = (Post.find_by(comments_postid: session[:comment_message_id]) || Challenge.find_by(comments_postid: session[:comment_message_id]))
        if !users.include?(c.user.id)
          send_to_telegram chat_id: c.user.uid, text: t('messages.new_comment',
            posturl: BotConst::Channel + '/' + session[:comment_message_id].to_s)
        end
      end
      return true
    end
  end

  def escape(text)
    if text
      text.gsub! '&', '&amp;'
      text.gsub! '>', '&gt;'
      text.gsub! '<', '&lt;'
      text.gsub! '"', '&quot;'
    end
    return text
  end

# def send_to_facebook
#   @graph = Koala::Facebook::API.new
#   @graph.put_wall_post('post on page wall')
# end

  ### WORKAROUND

  def answer_callback_query(*)
    super
  rescue Telegram::Bot::Error => e
    raise unless e.message.include?('QUERY_ID_INVALID')
    exlog.warn "BotCommon: Ignoring telegram error: #{e.message}"
  end

  def process(*)
    super
  rescue Telegram::Bot::Forbidden
    exlog.warn 'BotCommon: Reply failed due to stale chat.'
    message = update['message'] || update['channel_post'] ||
              update.dig('callback_query', 'message')
    send_to_telegram chat_id: BotConst::Admin, text: I18n.t('errors.format', message: 'BotCommon: forbidden chat_id ' + message&.dig('chat', 'id').to_s)
  end

  def edit_message(type, params = {})
    super
  rescue Telegram::Bot::Error => e
    raise unless e.message.include?('message is not modified')
    exlog.warn "BotCommon: Ignoring telegram error: #{e.message}"
  end

  def action_missing(action, *_args)
    if action_type == :command
      exlog.warn "BotCommon: Missing command: #{action_options[:command]}"
    else
      exlog.warn "BotCommon: Missing feature: #{action}"
    end
  end
end
