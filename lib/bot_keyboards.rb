module BotKeyboards

  ItemPerPage = 7

  def list_keyboard(position = 0, list = [])
    kbd = []
    kbd << buttons_prev_next('challenges', position, list.length / ItemPerPage) if list.length > ItemPerPage
    kbd += list.slice(position * ItemPerPage, ItemPerPage)
  end

  def rating_keyboard(list)
    list << [
      { text: t('buttons.other.langs'), callback_data: 'langs|' },
      { text: t('buttons.other.help'), callback_data: 'help|' }
    ]
    #if @user.auto_comments
    #  list << [{ text: (t('items.accept') + ' ' + t('buttons.other.auto_comments')), callback_data: 'auto_comments|' }]
    #else
    #  list << [{ text: t('buttons.other.auto_comments'), callback_data: 'auto_comments|' }]
    #end    
  end  

  def post_keyboard(post)
    likes = post.rates.likes.size > 0 ? t('items.like') + ' ' + post.rates.likes.size.to_s : t('items.like')
    unlikes = post.rates.unlikes.size > 0 ? t('items.unlike') + ' ' + post.rates.unlikes.size.to_s : t('items.unlike')
    kbd = []
    if post&.challenge.postid
      kbd << [{ text: post.challenge&.formatted_title,
        url: 't.me/' + BotConst::Channel + '/' + post.challenge.postid.to_s }]
    else
      kbd << [{ text: post.challenge&.formatted_title,
        callback_data: 'challenge|' + post.challenge.id.to_s }] if post.challenge.id != BotConst::Challenge_Start_Id
    end
    kbd << [
      { text: likes, callback_data: 'like|' + post.id.to_s },
      { text: unlikes, callback_data: 'unlike|' + post.id.to_s }
    ]
  end

  def start_keyboard
    [
      [t('buttons.main.list'), t('buttons.main.rating')]
    ]
  end

  def show_challenge_keyboard(challenge)
    if (session[:filetype] || session[:text])
      #{ text: t('buttons.back'), callback_data: 'back_to|' }
      [[
        { text: t('buttons.confirm_answer'),
          callback_data: 'answer|' + challenge.id.to_s }
      ]]
    elsif @user.challenges.exists? challenge.id
      autorenew = t('buttons.autorenew_challenge')
      if challenge.autorenew
        autorenew = t('items.accept') + ' ' + autorenew
      end

      [[
        { text: t('buttons.add_time', time: time_ago_in_words(BotConst::Time_To_Add.from_now)),
          callback_data: 'add_time|' + challenge.id.to_s },
        { text: t('buttons.add_person'),
          callback_data: 'add_person|' + challenge.id.to_s }
      ],
      [
        { text: autorenew,
          callback_data: 'autorenew_challenge|' + challenge.id.to_s }
      ],
      [
        { text: t('buttons.delete_challenge'),
          callback_data: 'delete_challenge|' + challenge.id.to_s }
      ],
      [
        { text: t('buttons.back'), callback_data: 'back_to|' }
      ]]
    else
      # TODO: May be, allow to delete some challenges?
      [[
        { text: t('buttons.back'), callback_data: 'back_to|' }
      ]]
    end
  end

  def skip_text_keyboard
    [[
      { text: t('buttons.skip_step'), callback_data: 'skip_text|' }
    ]]
  end

  def lang_keyboard
    [[
      { text: t('lang.ru'), callback_data: 'lang|ru' },
      { text: t('lang.uk'), callback_data: 'lang|uk' }
    ],[
      { text: t('buttons.back'), callback_data: 'back_to|' }
    ]]
  end

  def comment_keyboard
    [[
      #if @user
      #  { text: t('buttons.add_comment'), callback_data: 'add_comment|' + @user.uid.to_s }
      #else
      { text: I18n.t('buttons.bot_url'), url: 't.me/' + BotConst::Bot },
      { text: t('buttons.add_comment'), callback_data: 'add_comment|' }
      #end
    ]]
  end

  def bot_url_keyboard
    [[
      { text: I18n.t('buttons.bot_url'), url: 't.me/' + BotConst::Bot }
    ]]
  end

  def back_keyboard
    [[
      { text: t('buttons.back'), callback_data: 'back_to|' }
    ]]
  end

  private

  def button_add_challenge
    [
      { text: t('buttons.add_challenge'), callback_data: 'new_challenge|' }
    ]
  end

  def buttons_prev_next(callback, position = 0, maximum = 0)
    [
      { text: t('items.backward') + ' ' + Array.new(position, t('items.tick')).join,
        callback_data: callback + '|' + (position - 1 < 0 ? maximum : position - 1).to_s },
      { text: Array.new(maximum - position, t('items.tick')).join + ' ' + t('items.forward'),
        callback_data: callback + '|' + (position + 1 > maximum ? 0 : position + 1).to_s }
    ]
  end
end
