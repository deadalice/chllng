module Exlogger
  def exlog
    @@exlogger ||= Logger.new("log/#{Rails.env}.external.log", 5, 2.megabytes)
    @@exlogger.formatter ||= Rails.application.config.log_formatter
    @@exlogger
  end

  def _info(_obj)
    exlog.info "#{Time.current.to_s(:short)} #{_obj.class}:#{_obj.id} #{_obj.changes}"
  end

  def _error(_obj)
    exlog.error "#{Time.current.to_s(:short)} #{_obj.class} #{_obj.errors.full_messages.to_sentence}"
  end
end
