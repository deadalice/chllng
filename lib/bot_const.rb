module BotConst
  if Rails.env.production?
    Bot = 'cotdbot'
    Channel = 'chllng'
    Group = 'chllng_chat'
  else
    Bot = 'testcotdbot'
    Channel = 'chllng_feed'
    Group = 'chllng_feed'
  end

  Challenge_Start_Id = 1
  Admin = 4926969

  Text_Size_Limit = 900
  Title_Size_Limit = 46
  Comment_Limit = 10
  Comment_Size_Limit = 300

  #Bonus_Rating_For_Top_Answer = 10
  Post_Expiration_Time = 1.day
  Edit_Expiration_Time = 1.month
  Max_Challenges = 3
  Block_For_Bad_Post = 1.day
  Time_To_Add = 1.day
  Person_To_Add = 1
  Rate_Limit_For_Block = -5
  Block_For_Block = 1.month
  Operation_Time_Limit = 5.seconds

  Advert_Random = 5
end
